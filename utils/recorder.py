from torch.utils.tensorboard import SummaryWriter
import torch
import os
import time
import shutil
from datetime import datetime
from .utils import run_cmd


def mk_alldir(dirpath):
  dir_list = dirpath.split('/')
  path = ''
  for d in dir_list:
    path = os.path.join(path, d)
    if not os.path.exists(path):
      os.mkdir(path)

class Recorder(SummaryWriter):
  """
  Recorder is a class for saving and loading stats and big files (such as model checkpoints) to both local directory and the bucket.
  """
  def __init__(self, log_dir=None, bucket_dir="blueshift_curricula", comment='', max_queue=10, flush_secs=120, upload_secs=120, save_ckpt_freq=10, sep_ckpt_freq=1, upload_ckpt_freq=1):
    """
    Arguments:
    logdir: name of the file to be used both locally and in the bucket
    bucketdir: path to the bucket directory. This is typically the directory that includes all experiments related to a project.
    comment: log_dir suffix appended to the default log_dir. If log_dir is assigned, this argument has no effect. 
    max_queue: size of the queue for the light objects (that do not need much memory) before saving the stat in the file
    flush_secs: how often the light objects will be saved in the stat file.
    upload_secs: how often the stat file of the light objects will be uploaded to the bucket
    save_ckpt_freq: frequency of saving checkpoints
    sep_ckpt_freq: Every sep_ckpt_freq * save_ckpt_freq checkpoint will also be saved separately on the bucket
    upload_ckpt_freq: frequency of uploading saved checkpoints (every ep_ckpt_freq * save_ckpt_freq) to the bucket.
    """

    self.log_dir = log_dir
    if self.log_dir is None:
      now = datetime.now()
      self.log_dir = f"{os.uname()[1].split('.')[0]}/"
      self.log_dir += now.strftime("%Y/%m/%d/%H/%M/%S")
    if len(comment) > 0:
      self.log_dir += f'-{comment}'

    self.bucket_dir = os.environ.get('BUCKET_DIR') if bucket_dir is None else bucket_dir

    if self.bucket_dir is None:
      print('No bucketdir provided. The logs and checkpoints will be saved locally')
    else:
      print(f'Logs and checkpoints will be loaded to gs://{self.bucket_dir}')

    print(f'log directory: {self.log_dir}')

    self.run_local = f'runs/{self.log_dir}'
    self.run_bucket = f'gs://{self.bucket_dir}/run/{self.log_dir}'
    self.ckpt_local = f'checkpoints/{self.log_dir}'
    self.ckpt_bucket = f'gs://{self.bucket_dir}/checkpoints/{self.log_dir}'
    self.stat = {}
    self.max_queue = max_queue
    self.flush_secs = flush_secs
    self.upload_secs = upload_secs
    self.queue_size = 0
    self.last_flush = time.time()
    self.last_upload = time.time()
    self.save_ckpt_freq = save_ckpt_freq
    self.sep_ckpt_freq = sep_ckpt_freq
    self.upload_ckpt_freq = upload_ckpt_freq
    self.best_acc = 0

    mk_alldir(self.run_local)
    mk_alldir(self.ckpt_local)

    self.sp = run_cmd(f'gsutil -m cp {self.run_bucket}/* {self.run_local}')
    if self.sp.wait() == 0:
      print(f'Donwloaded previous run stat from {self.run_bucket}')

    self.stat = torch.load(f'{self.run_local}/stat.pth') if os.path.exists(f'{self.run_local}/stat.pth') else {}
    super().__init__(self.run_local, max_queue=max_queue, flush_secs=flush_secs)


  def add_objects(self, main_tag, tag_obj_dict, global_step=None, walltime=None):
    """
    This function can be used for saving a dictionary of light objects in a similar way to tensorboard.
    For heavy objects such as model checkpoints, use save_checkpoint
    """
    if main_tag not in self.stat:
      self.stat[main_tag] = {}
    for key, value in tag_obj_dict.items():
      if key not in self.stat[main_tag]:
        self.stat[main_tag][key] = {'step':[],'time':[], 'object':[]}
      self.stat[main_tag][key]['step'].append(global_step)
      self.stat[main_tag][key]['time'].append(walltime)
      self.stat[main_tag][key]['object'].append(value)
      self.queue_size += 1

    if self.queue_size >= self.max_queue or time.time() - self.last_flush >= self.flush_secs:
      torch.save(self.stat, f'{self.run_local}/stat.pth')
      if time.time() - self.last_upload >= self.upload_secs:
        print('uploading now')
        self.sp = run_cmd(f'gsutil -m cp {self.run_local}/* {self.run_bucket}', prev_sp=self.sp)
        self.last_upload = time.time()
      self.queue_size = 0
      self.last_flush = time.time()


  def add_object(self, tag, obj, global_step=None, walltime=None):
    """
    This function can be used for saving a light object in a similar way to tensorboard
    """
    self.add_objects(tag, {'value': obj}, global_step=global_step, walltime=walltime)


  def add_scalars(self, main_tag, tag_scalar_dict, global_step=None, walltime=None):
    """
    This functions extends tensorboards add_scalars function to objects
    """
    super().add_scalars(main_tag, tag_scalar_dict, global_step=global_step, walltime=walltime)
    self.add_objects(main_tag, tag_scalar_dict, global_step=global_step, walltime=walltime)

  def add_scalar(self, tag, scalar, global_step=None, walltime=None):
    """
    This functions extends tensorboards add_scalar function to objects
    """
    super().add_scalar(tag, scalar, global_step=global_step, walltime=walltime)
    self.add_object(tag, scalar, global_step=global_step, walltime=walltime)

  def add_losses(self, tr_loss, tr_acc1, tr_acc5, val_loss, val_acc1, val_acc5, global_step=None, walltime=None):
    self.add_scalar('Loss/train', tr_loss, global_step=global_step, walltime=walltime)
    self.add_scalar('Loss/test', val_loss, global_step=global_step, walltime=walltime)
    self.add_scalar('Top1Accuracy/train', tr_acc1, global_step=global_step, walltime=walltime)
    self.add_scalar('Top1Accuracy/test', val_acc1, global_step=global_step, walltime=walltime)
    self.add_scalar('Top5Accuracy/train', tr_acc5, global_step=global_step, walltime=walltime)
    self.add_scalar('Top5Accuracy/test', val_acc5, global_step=global_step, walltime=walltime)

  def add_losses_val(self, tr_loss, tr_acc1, tr_acc5,  trainval_loss, trainval_acc1, trainval_acc5, val_loss, val_acc1, val_acc5, global_step=None, walltime=None):
    self.add_scalar('Loss/train', tr_loss, global_step=global_step, walltime=walltime)
    self.add_scalar('Loss/trainval', trainval_loss, global_step=global_step, walltime=walltime)    
    self.add_scalar('Loss/test', val_loss, global_step=global_step, walltime=walltime)
    self.add_scalar('Top1Accuracy/train', tr_acc1, global_step=global_step, walltime=walltime)
    self.add_scalar('Top1Accuracy/trainval', trainval_acc1, global_step=global_step, walltime=walltime)    
    self.add_scalar('Top1Accuracy/test', val_acc1, global_step=global_step, walltime=walltime)
    self.add_scalar('Top5Accuracy/train', tr_acc5, global_step=global_step, walltime=walltime)
    self.add_scalar('Top5Accuracy/trainval', trainval_acc5, global_step=global_step, walltime=walltime)    
    self.add_scalar('Top5Accuracy/test', val_acc5, global_step=global_step, walltime=walltime)
    
    
  def add_train(self, train_loss, train_pred, global_step=None, walltime=None):
    state_dict = {'global_step': global_step,
#                   'walltime': walltime,
                  'IndLoss/train': train_loss,
                  'IndPred/train': train_pred }
    self.save_checkpoint(state_dict, prefix='train_samplePre', global_step=global_step,force_save=True) 
  def add_trainOut(self, out, global_step=None, walltime=None):
    state_dict = {'global_step': global_step,
#                   'walltime': walltime,
                  'IndOut/train': out}
    self.save_checkpoint(state_dict, prefix='train_sampleOut', global_step=global_step,force_save=True)   
  def add_test(self, val_loss, val_pred, global_step=None, walltime=None):
    state_dict = {'global_step': global_step,
#                   'walltime': walltime,
                  'IndLoss/test': val_loss ,
                  'IndPred/test': val_pred }
    self.save_checkpoint(state_dict, prefix='test_samplePre', global_step=global_step,force_save=True)   
  def add_batch(self, batches, global_step=None, walltime=None):
    state_dict = {'global_step': global_step,
#                   'walltime': walltime,
                  'batches': batches}
    self.save_checkpoint(state_dict, prefix='batch_sample', global_step=global_step,force_save=True)  
    
  def add_traintest(self, data, global_step=None, walltime=None):
    self.save_checkpoint(data, prefix='test_train', global_step=global_step,force_save=True)     
  def add_order(self, data, global_step=None, walltime=None):
    self.save_checkpoint(data, prefix='order', global_step=global_step,force_save=True)           
        
    
  def add_trainval(self, val_loss, val_pred, global_step=None, walltime=None):
    state_dict = {'global_step': global_step,
#                   'walltime': walltime,
                  'IndLoss/test': val_loss,
                  'IndPred/test': val_pred }
    self.save_checkpoint(state_dict, prefix='trainval_samplePre', global_step=global_step,force_save=True)   
    
  def add_testOut(self, val_out, global_step=None, walltime=None):
    state_dict = {'global_step': global_step,
#                   'walltime': walltime,
                  'IndOut/test': val_out}
    self.save_checkpoint(state_dict, prefix='test_sampleOut', global_step=global_step,force_save=True)    
  def get_object(self, main_tag, key=None):
    """
    Returning an object given the main tag and the key
    """
    return self.stat[main_tag][key]

  def save_checkpoint(self, state_dict, prefix='model', global_step=None, is_best=False,  force_save=False):
    """
    This function can be used for saving heavy objects such as checkponts.
    """
    src = f'{self.ckpt_local}/{prefix}_last.pth'
    if force_save:
      self.sp.wait()
      torch.save(state_dict, src)
      dest = f'{self.ckpt_bucket}/{prefix}_{global_step}.pth'
      self.sp = run_cmd(f'gsutil -o GSUtil:parallel_composite_upload_threshold=150M cp {src} {dest}', prev_sp=self.sp)
        
        
    if global_step is None or global_step % self.save_ckpt_freq == 0:
      src_best = f'{self.ckpt_local}/{prefix}_best.pth'
      torch.save(state_dict, src)
      if is_best:
        shutil.copyfile(src, src_best)
      if global_step is None or global_step % (self.save_ckpt_freq * self.upload_ckpt_freq) == 0:
        dest = f'{self.ckpt_bucket}/{prefix}_last.pth'
        self.sp = run_cmd(f'gsutil -o GSUtil:parallel_composite_upload_threshold=150M cp {src} {dest}', prev_sp=self.sp)
        if os.path.exists(src_best):
          dest_best = f'{self.ckpt_bucket}/{prefix}_best.pth'
          self.sp = run_cmd(f'gsutil -o GSUtil:parallel_composite_upload_threshold=150M cp {src_best} {dest_best}')
      elif global_step % (self.save_ckpt_freq * self.sep_ckpt_freq) == 0:
        self.sp.wait()
        dest = f'{self.ckpt_bucket}/{prefix}_{global_step}.pth'
        self.sp = run_cmd(f'gsutil -o GSUtil:parallel_composite_upload_threshold=150M cp {src} {dest}', prev_sp=self.sp)

  def load_checkpoint(self, prefix='model', global_step=None, log_dir=None):
    """
    This function loads a heavy objects from local directory or bucket
    """
    ckpt_dir = f'{self.log_dir}' if log_dir is None else f'checkpoints/{log_dir}'
    global_step = 'last' if global_step is None else global_step
    dest = f'{ckpt_dir}/{prefix}_{global_step}.pth'
    src = f'gs://self.bucket_dir/{ckpt_dir}/{prefix}_last.pth'
    if not os.path.exists(dest):
      self.sp = run_cmd(f'gsutil cp {src} {dest}', prev_sp=self.sp)
      if self.sp.wait() == 0:
        print(f'Checkpoint downloaded from {src} to {dest}')
    if os.path.exists(dest):
        ckpt = torch.load(dest)
        print(f'Checkpoint loaded from {dest}')
    else:
        print(f'No Checkpoint was found in {dest} or {src}')
        ckpt = None
    return ckpt

  def save_full_checkpoint(self, model, optimizer, scheduler, args, epoch, acc):
    is_best = acc > self.best_acc
    self.best_acc = max(is_best, self.best_acc)
    try:
        self.save_checkpoint({
                    'epoch': epoch,
                    'args':args,
                    'best_acc': self.best_acc,
                    'state_dict': model.state_dict(),
                    'optimizer' : optimizer.state_dict(),
                    'scheduler': scheduler.state_dict()
        }, global_step = epoch, is_best=is_best)
    except AttributeError:
        print ("AttributeError")
        self.save_checkpoint({
                    'epoch': epoch,
                    'args':args,
                    'best_acc': self.best_acc,
                    'state_dict': model.state_dict(),
                    'optimizer' : optimizer.state_dict()
        }, global_step = epoch, is_best=is_best)       

#   def resume_full_checkpoint(self, resume, model, optimizer, scheduler):
#     if resume:
#       if resume == 'previous':
#         full_dict = self.load_checkpoint()
#       else:
#         full_dict = self.load_checkpoint(log_dir=resume)
#       model.load_state_dict(full_dict['state_dict'])
#       optimizer.load_state_dict(full_dict['optimizer'])
#       scheduler.load_state_dict(full_dict['scheduler'])
#       self.best_acc = full_dict['best_acc']
#       return full_dict['epoch']
#     return 0

  def resume_full_checkpoint(self, resume, model, optimizer, scheduler):
    if resume:
      if resume == 'previous':
        full_dict = self.load_checkpoint()
      else:
        full_dict =  torch.load(resume)#self.load_checkpoint(log_dir=resume)
      model.load_state_dict(full_dict['state_dict'])
      optimizer.load_state_dict(full_dict['optimizer'])
      scheduler.load_state_dict(full_dict['scheduler'])
      self.best_acc = full_dict['top1_acc']
      return full_dict['epoch'],model,optimizer, scheduler,self.best_acc
    return 0


  def close(self):
    super().flush()
    super().close()
    torch.save(self.stat, f'{self.run_local}/stat.pth')
    self.sp = run_cmd(f'gsutil -m cp {self.run_local}/* {self.run_bucket}', prev_sp=self.sp)
    self.sp.wait()
