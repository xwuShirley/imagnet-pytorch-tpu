
from torch.utils.data.sampler import Sampler
class CurrSampler(Sampler):
    r"""Samples elements randomly, without replacement.
    Arguments:
        data_source (Dataset): dataset to sample from
    """
    def __init__(self,curricula_index, num_batches, batch_size):
        # curricula_index is a dictionary
        
        self.batch_size = batch_size
        self.num_batches = num_batches
        self.curricula_index = curricula_index
        self.next_idx = 0
    
    def __iter__(self):
        while self.num_batches > 0:
            yield self.curricula_index[self.next_idx]
            self.num_batches -=1            
            self.next_idx += 1
    def __len__(self):
        return self.num_batches*self.batch_size 

class CurrBatchSampler(Sampler):

    def __init__(self, sampler, batch_size, drop_last):
        self.sampler = sampler
        self.batch_size = batch_size
        self.drop_last = drop_last

    def __iter__(self):
        batch = []
        for _, idx in enumerate(iter(self.sampler)):
            batch = idx
            yield batch

        if len(batch) > 0 and not self.drop_last:
            yield batch

    def __len__(self):
        return len(self.sampler) // self.batch_size